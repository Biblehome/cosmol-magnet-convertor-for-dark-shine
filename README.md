# DarkSHINE magnet package

[[_TOC_]]

## How to setup & run

To install the codes:
```shell script
mkdir [YOUR_WORKSPACE]
cd [YOUR_WORKSPACE]
git clone git@gitlab.com:Biblehome/cosmol-magnet-convertor-for-dark-shine.git source
mkdir build install run
source source/setup.sh
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install ../source
make
make install
```

Each time you open a new terminal, in YOUR\_WORKSPACE run:
```shell script
source source/setup.sh
```
This will set up ROOT environment and the codes

To run the codes, first create the example magnet. In YOUR\_WORKSPACE:
```shell script
cd run
magnet_example
```
You should see the following information if installed correctly:
```shell script
INFO    Example magnet file [YOUR_WORKSPACE]/source/magnet/test_magnet_x.txt saved
INFO    Example magnet file [YOUR_WORKSPACE]/source/magnet/test_magnet_y.txt saved
INFO    Example magnet file [YOUR_WORKSPACE]/source/magnet/test_magnet_z.txt saved
```
These txt files are stored in source/magnet by default

Then, run the convertor:
```shell script
cp ../install/config.yaml .
magnet_convertor config.yaml
```

A ROOT file should be created. Open it and run:
```shell script
magnet1->GetField(x, y, z)
```
This converts the example magnet txt files into DMagnet. The magnet distribution should be like:
```shell script
    -800                 0       250 z[mm]
   0  _____________________________
      |                          /
      |                  |      /
      |                        /
      |                  |    /
      |                      /
      |                  |  /
      |                    /
-1.5  |__________________|/
B[T]  |
```
