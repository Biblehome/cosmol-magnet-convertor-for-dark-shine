#include <iostream>
#include <string>
#include <vector>

#include "ConfigReader.h"

std::vector<std::string> inputs;
std::string output;

std::string mode;
std::vector<double> xRange;
std::vector<double> yRange;
std::vector<double> zRange;

std::vector<int> mPowers;
int mFunctions;
int mStudy;
int mTerms;
double pLimit;
double minAngle;
double maxAngle;
double mRelativeError;

ConfigReader::ConfigReader(std::string config) : config_(config)
{
}

bool ConfigReader::Read()
{
    YAML::Node file = YAML::LoadFile(config_);

    try
    {
        mode = file["Mode"].as<std::string>();
        if(mode == "normal")
            inputs = file["Inputs"].as<std::vector<std::string>>();

        output = file["Output"].as<std::string>();

        xRange = file["XRange"].as<std::vector<double>>();
        yRange = file["YRange"].as<std::vector<double>>();
        zRange = file["ZRange"].as<std::vector<double>>();

        mPowers = file["MaxPowers"].as<std::vector<int>>();
        mFunctions = file["MaxFunctions"].as<int>();
        mStudy = file["MaxStudy"].as<int>();
        mTerms = file["MaxTerms"].as<int>();
        pLimit = file["PowerLimit"].as<double>();
        minAngle = file["MinAngle"].as<double>();
        maxAngle = file["MaxAngle"].as<double>();
        mRelativeError = file["MinRelativeError"].as<double>();

        for(int i = 0; i < xRange.size() - 1; i++)
        {
            if(xRange.at(i) >= xRange.at(i + 1))
            {
                std::cout << "WARNING	We need increasing binning in x" << std::endl;
                return false;
            }
        }
        for(int j = 0; j < yRange.size() - 1; j++)
        {
            if(yRange.at(j) >= yRange.at(j + 1))
            {
                std::cout << "WARNING	We need increasing binning in y" << std::endl;
                return false;
            }
        }
        for(int k = 0; k < zRange.size() - 1; k++)
        {
            if(zRange.at(k) >= zRange.at(k + 1))
            {
                std::cout << "WARNING	We need increasing binning in z" << std::endl;
                return false;
            }
        }

        std::cout << std::endl;
        std::cout << "User config:" << std::endl
                  << "----------------" << std::endl;
        std::cout << " Input files:			";
        for(auto input : inputs)
           std::cout << input << " ";
        std::cout << std::endl;
        std::cout << " Output file:			" << output << ".root" << std::endl;
        std::cout << " Mode:				" << mode   << std::endl;
        std::cout << " X bins:			( ";
        for(auto x : xRange)
            std::cout << x << " ";
        std::cout << ")" << std::endl;
        std::cout << " Y bins:			( ";
        for(auto y : yRange)
            std::cout << y << " ";
        std::cout << ")" << std::endl;
        std::cout << " Z bins:			( ";
        for(auto z : zRange)
            std::cout << z << " ";
        std::cout << ")" << std::endl;

        std::cout << " Max Powers:			( ";
        for(auto p : mPowers)
            std::cout << p << " ";
        std::cout << ")" << std::endl;
        std::cout << " MaxFunctions:			" << mFunctions     << std::endl;
        std::cout << " MaxStudy:			" << mStudy         << std::endl;
        std::cout << " MaxTerms:			" << mTerms         << std::endl;
        std::cout << " PowerLimit:			" << pLimit         << std::endl;
        std::cout << " MinAngle:			" << minAngle       << std::endl;
        std::cout << " MaxAngle:			" << maxAngle       << std::endl;
        std::cout << " MinRelativeError:		" << mRelativeError << std::endl;

    }
    catch(YAML::BadConversion &e)
    {
        std::cerr << "WARNING	" << e.msg << std::endl;
        return false;
    }
    catch(YAML::InvalidNode &e)
    {
        std::cerr << "WARNING	" << e.msg << std::endl;
        return false;
    }

    std::cout << std::endl;

    return true;
}
