#include <iostream>

#include "Util.h"
#include "MagnetReader.h"

MagnetReader::MagnetReader(TString name) : name_(name)
{}

void MagnetReader::Read(std::vector<double> &x,
                        std::vector<double> &y,
                        std::vector<double> &z,
                        std::vector<double> &B)
{
    std::vector<double>().swap(x);
    std::vector<double>().swap(y);
    std::vector<double>().swap(z);
    std::vector<double>().swap(B);

    file_.open(name_, std::ios::in);

    if (!file_.is_open())
    {
        std::cerr << "ERROR	fail to open " << name_ << std::endl;
        exit(0);
    }

    TString flag;
    while(flag.ReadLine(file_))
    {
        if(!flag.Contains("%"))
        {
           TString flagToSplit(flag);

           std::vector<TString> map;
           SplitString(flagToSplit, ' ', map);
           if(map.size() != 4)
           {
               map.clear();
               SplitString(flagToSplit, '\t', map);
           }

           if(map.size() == 4)
           {
               if(map.at(0).IsFloat() &&
                  map.at(1).IsFloat() &&
                  map.at(2).IsFloat() &&
                  map.at(3).IsFloat())
               {
                   x.emplace_back(map.at(0).Atof());
                   y.emplace_back(map.at(1).Atof());
                   z.emplace_back(map.at(2).Atof());
                   B.emplace_back(map.at(3).Atof());
               }
               else
               {
                   std::cout << "WARNING	Abnormal number" << std::endl;
                   continue;
               }
           }
           else
           {
               std::cout << "WARNING	Abnormal line: " << flag << std::endl;
               continue;
           }
        }
    }
}
