#include <iostream>
#include <cmath>

#include "TDirectory.h"

#include "Globals.h"
#include "MultiDimFitter.h"

//public:
void MultiDimFitter::Init()
{
    fitter_ = new TMultiDimFit(3, TMultiDimFit::kMonomials, "v");

    int mPowers_array[] = {mPowers.at(0), mPowers.at(1), mPowers.at(2)}; 
    fitter_->SetMaxPowers(mPowers_array);
    fitter_->SetMaxFunctions(mFunctions);
    fitter_->SetMaxStudy(mStudy);
    fitter_->SetMaxTerms(mTerms);
    fitter_->SetPowerLimit(pLimit);
    fitter_->SetMinAngle(minAngle);
    fitter_->SetMaxAngle(maxAngle);
    fitter_->SetMinRelativeError(mRelativeError);

    fitter_->Print("p");
}

void MultiDimFitter::SetName(const TString &name)
{
    fitter_->SetName(name);
}

void MultiDimFitter::SetTitle(const TString &title)
{
    fitter_->SetTitle(title);
}

void MultiDimFitter::SetActiveRange(const double &xDown, const double &xUp,
                                    const double &yDown, const double &yUp,
                                    const double &zDown, const double &zUp)
{
    xUp_ = xUp;
    yUp_ = yUp;
    zUp_ = zUp;
    xDown_ = xDown;
    yDown_ = yDown;
    zDown_ = zDown;
}

void MultiDimFitter::AddRow(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &z,
                            const std::vector<double> &B)
{
    if(!fitter_)
    {
        std::cout << "WARNING	No fitter to add rows" << std::endl;
        return;
    }

    x_ = &x;
    y_ = &y;
    z_ = &z;
    B_ = &B;

    auto it = x_->begin();
    for(; it != x_->end(); it++)
    {
        int i = it - x_->begin();
        if(x_->at(i) < xUp_ && x_->at(i) > xDown_ &&
           y_->at(i) < yUp_ && y_->at(i) > yDown_ &&
           z_->at(i) < zUp_ && z_->at(i) > zDown_)
        {
            double coor[] = {x_->at(i), y_->at(i), z_->at(i)};
            fitter_->AddRow(coor, B_->at(i), 0.);
        }
//        else
//            std::cerr << "WARNING	Magnet crosses the border" << std::endl;
    }
}

void MultiDimFitter::Fit()
{
    if(!fitter_)
    {
        std::cout << "WARNING	No fitter to fit" << std::endl;
        return;
    }

    fitter_->Print("s");
    fitter_->MakeHistograms();
    fitter_->FindParameterization();
    fitter_->Print("rc");
}

void MultiDimFitter::Evaluate()
{
    if(!fitter_)
    {
        std::cout << "WARNING	No fitter to evaluate" << std::endl;
        return;
    }

    double err_ = 0;

    double xMin  = (*fitter_->GetMinVariables())[0];
    double xMax  = (*fitter_->GetMaxVariables())[0];
    double yMin  = (*fitter_->GetMinVariables())[1];
    double yMax  = (*fitter_->GetMaxVariables())[1];
    double zMin  = (*fitter_->GetMinVariables())[2];
    double zMax  = (*fitter_->GetMaxVariables())[2];

    double xMean = 0.5*(xMax + xMin);
    double yMean = 0.5*(yMax + yMin);
    double zMean = 0.5*(zMax + zMin);

    std::cout << "INFO	Errors greater than 0.1 at:" << std::endl;
    auto it = x_->begin();
    for(; it != x_->end(); it++)
    {
        int i = it - x_->begin();
        double x = x_->at(i);
        double y = y_->at(i);
        double z = z_->at(i);

        if(std::abs(x - xMean)/(xMax - xMin) < 0.4 &&
           std::abs(y - yMean)/(yMax - yMin) < 0.4 &&
           std::abs(z - zMean)/(zMax - zMin) < 0.4)
        {
            double coor[] = {x, y, z};
    
            double truth = B_->at(i);
            double eval  = fitter_->Eval(coor);
            double err   = std::abs(truth - eval)/truth;
            err_ += err*err;

            if(err > 0.01)
                std::cout << "INFO	"
                          << "(" << x_->at(i) << "," << y_->at(i) << "," << z_->at(i) << ")	"
                          << truth            << "	"
                          << eval             << "	"
                          << err              << std::endl;

        }
    }

    std::cout << "INFO	General error: " << sqrt(err_)/x_->size() << std::endl;
    std::cout << "INFO	Reduced chi2: " << fitter_->GetChi2()/(fitter_->GetSampleSize() - fitter_->GetNCoefficients()) << std::endl;
}

TMultiDimFit* MultiDimFitter::GetFitter() const
{
    if(!fitter_)
        std::cout << "WARNING	No fitter found" << std::endl;

    return fitter_;
}

void MultiDimFitter::Write()
{
    bool ifRoot = false;

    TDirectory *dir = gDirectory;
    for(; dir; dir = dir->GetMotherDir())
    {
        TString name = dir->GetName();
        if(name.EndsWith(".root"))
        {
            ifRoot = true;
            break;
        }
    }
    
    if(!ifRoot)
    {
        std::cout << "WARNING	No root file to write fitter in" << std::endl;
        return;
    }

    if(fitter_)
    {
        fitter_->Write();
        saved_ = true;
    }
    else
        std::cout << "WARNING	No fitter to save" << std::endl;
}

//private:
void MultiDimFitter::Clear()
{
    if(fitter_)
        fitter_->Clear();

    delete fitter_;
    fitter_ = nullptr;
}
