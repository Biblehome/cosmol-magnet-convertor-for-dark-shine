#include <iostream>

#include "Util.h"

bool SplitString(const TString& theOpt, const char separator,
                 std::vector<TString> &v)
{
    v.clear();

    TString splitOpt(theOpt);
    splitOpt.ReplaceAll("\n", " ");
    splitOpt = splitOpt.Strip(TString::kBoth, separator); //Split splitOpt into TSubStrings by seperator

    while(splitOpt.Length() > 0)
    {
        if(!splitOpt.Contains(separator))
        {
            v.push_back(splitOpt);
            break;
        }
        //If the TSubString contains the seperator, fill v from 1st char to 1st seperator
        else
        {
            TString toSave = splitOpt(0, splitOpt.First(separator));
            v.push_back(toSave);
            splitOpt = splitOpt(splitOpt.First(separator), splitOpt.Length()); //Discard the chars added to the vector
        }
        splitOpt = splitOpt.Strip(TString::kLeading, separator);
    }

    return true;
}

