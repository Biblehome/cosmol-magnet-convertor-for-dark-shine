#include <iostream>
#include <vector>

#include "ExampleMagnetStream.h"

int main()
{
    std::vector<int> directions = {Bx, By, Bz};

    for(auto direction : directions)
    {
        ExampleMagnetStream stream(direction);
    
        stream.Open();
        stream.Write();
        stream.Close();
    }
}
