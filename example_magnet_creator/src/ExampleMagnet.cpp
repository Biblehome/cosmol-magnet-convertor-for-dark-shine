#include "ExampleMagnet.h"

//public:
std::ostream &operator <<(std::ostream &os, const ExampleMagnet &mag)
{
    ExampleMagnet magVar = const_cast<ExampleMagnet&>(mag);

    for(int i = 0; i < mag.xBin_ + 1; i++)
    {
        double xx = (mag.xUp_ - mag.xDown_)/mag.xBin_*i + mag.xDown_;

        for(int j = 0; j < mag.yBin_ + 1; j++)
        {
            double yy = (mag.yUp_ - mag.yDown_)/mag.yBin_*j + mag.yDown_;

            for(int k = 0; k < mag.zBin_ + 1; k++)
            {
                double zz = (mag.zUp_ - mag.zDown_)/mag.zBin_*k + mag.zDown_;
                double mm = magVar.GetExampleMagnet(xx, yy, zz);

                os << xx << "          " << yy << "          " << zz << "          " << mm << std::endl;
            }
        }
    }

    return os;
}

double ExampleMagnet::GetExampleMagnet(double x, double y, double z)
{
    if(direction_ == 1)
        return Func(x, y, z);

    return 0.;
}

//private:
double ExampleMagnet::Func(double x, double y, double z)
{
    double mag;

    if(x >= xDown_ && x <= xUp_ &&
       y >= yDown_ && y <= yUp_ &&
       z >= zDown_ && z < zTurning_)
        mag = -1.5;
    else if(x >= xDown_ && x <= xUp_ &&
            y >= yDown_ && y <= yUp_ &&
            z >= zTurning_ && z <= zUp_)
        mag = -1.5/(zUp_ - zTurning_)*(zTurning_ - z) - 1.5;

    return mag;
}

