#include <iostream>

#include "ExampleMagnetStream.h"

void ExampleMagnetStream::Open()
{
    if(direction_ == Bx)
    {
#ifndef TEST_MAGNET_FILE_X
        std::string TEST_MAGNET_FILE_X("test_magnet_x.txt");
        std::cout << "INFO	Please move test_magnet_x.txt into magnet/" << std::endl;
#endif
        name_ = TEST_MAGNET_FILE_X;
        out_.open(TEST_MAGNET_FILE_X);
    }
    else if(direction_ == By)
    {
#ifndef TEST_MAGNET_FILE_Y
        std::string TEST_MAGNET_FILE_Y("test_magnet_y.txt");
        std::cout << "INFO	Please move test_magnet_y.txt into magnet/" << std::endl;
#endif
        name_ = TEST_MAGNET_FILE_Y;
        out_.open(TEST_MAGNET_FILE_Y);
    }
    else if(direction_ == Bz)
    {
#ifndef TEST_MAGNET_FILE_Z
        std::string TEST_MAGNET_FILE_Z("test_magnet_z.txt");
        std::cout << "INFO	Please move test_magnet_z.txt into magnet/" << std::endl;
#endif
        name_ = TEST_MAGNET_FILE_Z;
        out_.open(TEST_MAGNET_FILE_Z);
    }

}

void ExampleMagnetStream::Write()
{
    if(!out_.is_open())
        std::cerr << "WARNING	No file to write" << std::endl;

    out_ << mag_;
}

void ExampleMagnetStream::Close()
{
    if(out_.is_open())
    {
        out_.close();
        std::cout << "INFO	Example magnet file " << name_ << " saved" << std::endl;
    }
    else
        std::cout << "WARNING	No magnet file to save" << std::endl;
}
