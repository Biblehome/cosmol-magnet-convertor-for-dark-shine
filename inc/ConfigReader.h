#ifndef YAML_READER_H
#define YAML_READER_H

#include <string>

#include "yaml-cpp/yaml.h"

#include "Globals.h"

class ConfigReader
{
public:
    ConfigReader(std::string config);
    ~ConfigReader() {}

    bool Read();

private:
    std::string config_;
};

#endif
