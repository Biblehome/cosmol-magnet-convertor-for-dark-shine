#ifndef UTIL_H
#define UTIL_H

#include <vector>

#include "TString.h"

bool SplitString(const TString& theOpt, const char separator, std::vector<TString> &v);

#endif
