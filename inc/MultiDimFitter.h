#ifndef MULTI_DIM_FITTER_H
#define MULTI_DIM_FITTER_H

#include <vector>

#include "TString.h"
#include "TMultiDimFit.h"

class MultiDimFitter
{
public:
    MultiDimFitter() {}
    ~MultiDimFitter() {this->Clear();}

    void Init();
    void SetName(const TString &name);
    void SetTitle(const TString &title);
    void SetActiveRange(const double &xDown, const double &xUp,
                        const double &yDown, const double &yUp,
                        const double &zDown, const double &zUp);
    void AddRow(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &z,
                const std::vector<double> &B);
    void Fit();
    void Evaluate();

    TMultiDimFit* GetFitter() const;
    void Write();

private:
    void Clear();

    TMultiDimFit *fitter_ = nullptr;
    const std::vector<double> *x_;
    const std::vector<double> *y_;
    const std::vector<double> *z_;
    const std::vector<double> *B_;

    double xUp_;
    double yUp_;
    double zUp_;
    double xDown_;
    double yDown_;
    double zDown_;

    bool saved_ = false;
};

#endif
