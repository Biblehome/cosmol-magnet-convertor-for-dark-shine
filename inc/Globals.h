#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include <vector>

extern std::vector<std::string> inputs;
extern std::string output;

extern std::string mode;
extern std::vector<double> xRange;
extern std::vector<double> yRange;
extern std::vector<double> zRange;

extern std::vector<int> mPowers;
extern int mFunctions;
extern int mStudy;
extern int mTerms;
extern double pLimit;
extern double minAngle;
extern double maxAngle;
extern double mRelativeError;

#endif
