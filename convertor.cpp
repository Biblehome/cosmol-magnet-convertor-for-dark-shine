#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

#include "TString.h"
#include "TFile.h"

#include "Globals.h"
#include "Util.h"
#include "MagnetReader.h"
#include "ConfigReader.h"
#include "MultiDimFitter.h"
#include "DMagnet.h"

int main(int argc, char **argv)
{
    ConfigReader config("config.yaml");
    if(!config.Read())
    {
        std::cout << "WARNING	Problem in reading config file" << std::endl;
        return -1;
    }

    std::vector<std::string> files(3);
    if(mode == "normal")
    {
#ifndef MAGNET_FILE_PATH
        files.at(0) = inputs.at(0);
        files.at(1) = inputs.at(1);
        files.at(2) = inputs.at(2);
#else
        files.at(0) = (std::string)MAGNET_FILE_PATH + "/" + inputs.at(0);
        files.at(1) = (std::string)MAGNET_FILE_PATH + "/" + inputs.at(1);
        files.at(2) = (std::string)MAGNET_FILE_PATH + "/" + inputs.at(2);
#endif
    }
    else if(mode == "test")
    {
#ifndef TEST_MAGNET_FILE_X
        std::cerr << "WARNING	No test magnet file Bx provided, run example first" << std::endl;
        return -1;
#else
        files.at(0) = TEST_MAGNET_FILE_X;
#endif
#ifndef TEST_MAGNET_FILE_Y
        std::cerr << "WARNING	No test magnet file By provided, run example first" << std::endl;
        return -1;
#else
        files.at(1) = TEST_MAGNET_FILE_Y;
#endif
#ifndef TEST_MAGNET_FILE_Z
        std::cerr << "WARNING	No test magnet file Bz provided, run example first" << std::endl;
        return -1;
#else
        files.at(2) = TEST_MAGNET_FILE_Z;
#endif
  }
    else
    {
        std::cerr << "WARNING	Mode " << mode << " not identified" << std::endl;
        return -1;
    }

    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;
    std::vector<double> B;

    TString outputName(output);
    outputName += ".root";
    TFile *output = new TFile(outputName, "recreate");

    int magId = 0;
    for(const auto &file : files)
    {
        MagnetReader reader(file);
        reader.Read(x, y, z, B);
    
        DMagnet mag("magnet" + std::to_string(magId), "magnet" + std::to_string(magId));
        mag.SetXDivision(xRange);
        mag.SetYDivision(yRange);
        mag.SetZDivision(zRange);
    
        for(int i = 0; i < xRange.size() - 1; i++)
        {
            for(int j = 0; j < yRange.size() - 1; j++)
            {
                for(int k = 0; k < zRange.size() - 1; k++)
                {
                    TString name = "fitter_" + std::to_string(i) + "_" + std::to_string(j) + "_" + std::to_string(k);
    
                    MultiDimFitter fitter;
                    fitter.Init();
                    fitter.SetName(name);
                    fitter.SetTitle(name);
                    fitter.SetActiveRange(xRange.at(i), xRange.at(i + 1),
                                          yRange.at(j), yRange.at(j + 1),
                                          zRange.at(k), zRange.at(k + 1));
                    fitter.AddRow(x, y, z, B);
                    fitter.Fit();
                    //fitter.Evaluate();
                    mag.AddMagnet(fitter.GetFitter());
    
                    //fitter.Write();
                }
            }
        }
    
        mag.Write();

        magId++;
    }

    output->Close();

    return 0;
}
